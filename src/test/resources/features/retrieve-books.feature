Feature: Retrieve books

  Scenario: Retrieve books

    Given The following table book content
      | id                                   | title             |
      | 29377c1a-7698-4639-b458-9e87962bc436 | The Da Vinci Code |
      | 40cff839-638c-47d4-ab7f-4021d78dc4b3 | Sapiens           |

    When I retrieve books

    Then I should have the following books response HTTP code "200"
    And I should have the following books response content
    """
[
  {
    "id": "29377c1a-7698-4639-b458-9e87962bc436",
    "title": "The Da Vinci Code"
  },
  {
    "id": "40cff839-638c-47d4-ab7f-4021d78dc4b3",
    "title": "Sapiens"
  }
]
    """