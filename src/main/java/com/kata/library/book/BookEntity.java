package com.kata.library.book;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.util.UUID;
@Getter
@Entity
@Table(name = "book")
public class BookEntity {

    @Id
    @NotNull
    private UUID id;

    @NotNull
    private String title;
}
