Feature: Retrieve users

  Scenario: Retrieve users

    Given The following table user content
      | id                                   | firstname | lastname | email                | subscriptionDate |
      | 6264391f-e016-4875-a311-1e1f2253a695 | John      | Doe      | john.doe@gmail.com   | 2023-01-01       |
      | 873118cf-facb-422b-b099-6b521496b082 | Emma      | Smith    | emma.smith@gmail.com | 2023-04-17       |

    When I retrieve users

    Then I should have the following users response HTTP code "200"
    And I should have the following users response content
    """
[
  {
    "id": "6264391f-e016-4875-a311-1e1f2253a695",
    "firstname": "John",
    "lastname": "Doe",
    "email": "john.doe@gmail.com",
    "subscriptionDate": "2023-01-01"
  },
  {
    "id": "873118cf-facb-422b-b099-6b521496b082",
    "firstname": "Emma",
    "lastname": "Smith",
    "email": "emma.smith@gmail.com",
    "subscriptionDate": "2023-04-17"
  }
]
    """