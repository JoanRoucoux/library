package com.kata.library.cucumber;


import com.kata.library.book.BookEntity;
import com.kata.library.user.UserEntity;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class RetrieveBooksStepDef extends ContextLoader {


    @Before
    public void setup() {
        bookRepository.deleteAll();
    }

    @Given("^The following table book content")
    public void given_the_following_table_book_content(List<BookEntity> bookEntityList) {
        bookRepository.saveAll(bookEntityList);
    }

    @When("^I retrieve books")
    public void i_retrieve_books() throws Exception {
        mvcPerform = mockMvc.perform(MockMvcRequestBuilders.get("/v1/books"));
    }

    @Then("^I should have the following books response HTTP code \"([^\"]*)\"")
    public void i_should_have_the_following_users_response_http_code(int httpCode) throws Exception {
        mvcPerform.andExpect(status().is(httpCode));

    }

    @Then("^I should have the following books response content")
    public void i_should_have_the_following_response_content(String expectedUsersJson) throws Exception {
        var actualResponseJson = mvcPerform.andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals(expectedUsersJson, actualResponseJson, JSONCompareMode.STRICT);
    }
}
