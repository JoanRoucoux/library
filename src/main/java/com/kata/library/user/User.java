package com.kata.library.user;

import java.time.LocalDate;
import java.util.UUID;

public record User(

        UUID id,

        String firstname,

        String lastname,

        String email,

        LocalDate subscriptionDate

) {
}
