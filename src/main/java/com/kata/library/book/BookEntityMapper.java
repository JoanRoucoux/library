package com.kata.library.book;


import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BookEntityMapper {

    Book convert(BookEntity bookEntity);

}
