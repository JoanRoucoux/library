package com.kata.library.book;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final BookEntityMapper bookEntityMapper;

    public BookService(BookRepository bookRepository, BookEntityMapper bookEntityMapper) {
        this.bookRepository = bookRepository;
        this.bookEntityMapper = bookEntityMapper;
    }


    List<Book> getBooks() {
        return bookRepository.findAll()
                .stream().map(bookEntityMapper::convert)
                .collect(Collectors.toList());
    }
}
