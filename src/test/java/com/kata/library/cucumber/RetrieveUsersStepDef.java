package com.kata.library.cucumber;


import com.kata.library.user.UserEntity;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class RetrieveUsersStepDef extends ContextLoader {


    @Before
    public void setup() {
        userRepository.deleteAll();
    }

    @Given("^The following table user content")
    public void given_the_following_table_user_content(List<UserEntity> userEntityList) {
        userRepository.saveAll(userEntityList);
    }

    @When("^I retrieve users")
    public void i_retrieve_users() throws Exception {
        mvcPerform = mockMvc.perform(MockMvcRequestBuilders.get("/v1/users"));
    }

    @Then("^I should have the following users response HTTP code \"([^\"]*)\"")
    public void i_should_have_the_following_users_response_http_code(int httpCode) throws Exception {
        mvcPerform.andExpect(status().is(httpCode));

    }

    @Then("^I should have the following users response content")
    public void i_should_have_the_following_response_content(String expectedUsersJson) throws Exception {
        var actualResponseJson = mvcPerform.andReturn().getResponse().getContentAsString();
        JSONAssert.assertEquals(expectedUsersJson, actualResponseJson, JSONCompareMode.STRICT);
    }
}
